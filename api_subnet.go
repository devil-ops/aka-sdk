package aka

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
)

// ListSubnets lists all subnets
func (c *Client) ListSubnets(ctx context.Context) (*Subnets, error) {
	items := &Subnets{}
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/ip4/subnets", c.baseURL), nil),
		items,
	); err != nil {
		return nil, err
	}
	return items, nil
}

// GetSubnet returns a specific subnet from a given CIDR
func (c *Client) GetSubnet(ctx context.Context, cidr string) (*SubnetWithTags, error) {
	item := &SubnetWithTags{}
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/ip4/subnets/%s", c.baseURL, cidr), nil),
		item,
	); err != nil {
		return nil, err
	}
	return item, nil
}

// GetSubnetTags retrieves all the tags for a given aka subnet
func (c *Client) GetSubnetTags(ctx context.Context, cidr string) (*map[string]string, error) {
	item := &map[string]string{}
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/ip4/subnets/%s/tags", c.baseURL, cidr), nil),
		item); err != nil {
		return nil, err
	}
	return item, nil
}

// GetSubnetByTag returns subnets matching a given tag name
func (c *Client) GetSubnetByTag(ctx context.Context, tagName string) (*Subnets, error) {
	p, err := json.Marshal(map[string]string{
		"tag": tagName,
	})
	if err != nil {
		return nil, err
	}
	items := &Subnets{}
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/ip4/subnets", c.baseURL), bytes.NewBuffer(p)),
		items); err != nil {
		return nil, err
	}
	return items, nil
}
