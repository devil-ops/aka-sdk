package aka

import (
	"bytes"
	"context"
	"fmt"
)

// ReadIP reads a given ip
func (c *Client) ReadIP(ctx context.Context, ip string) (*IP, error) {
	item := &IP{}
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/ip/%s", c.baseURL, ip), nil),
		item,
	); err != nil {
		return nil, err
	}

	return item, nil
}

// CreateIP creats a new IP entry
func (c *Client) CreateIP(ctx context.Context, ip string, params *IPParams) (*UpdateIPResponse, error) {
	res := &UpdateIPResponse{}
	if err := c.sendRequest(
		mustNewRequest("POST", fmt.Sprintf("%s/api/v1/ip/%s", c.baseURL, ip), bytes.NewBuffer(mustMarshal(params))),
		res,
	); err != nil {
		return nil, err
	}
	return res, nil
}

// UpdateIP updates a given ip with the given params
func (c *Client) UpdateIP(ctx context.Context, params UpdateIPParams, ip string) (*UpdateIPResponse, error) {
	req := mustNewRequest("PUT", fmt.Sprintf("%s/api/v1/ip/%s", c.baseURL, ip), bytes.NewBuffer(mustMarshal(params)))

	res := &UpdateIPResponse{}
	if err := c.sendRequest(req, res); err != nil {
		return nil, err
	}
	return res, nil
}

// DeleteIP deletes the given ip
func (c *Client) DeleteIP(ctx context.Context, ip string) (*UpdateIPResponse, error) {
	res := &UpdateIPResponse{}
	if err := c.sendRequest(
		mustNewRequest("DELETE", fmt.Sprintf("%s/api/v1/ip/%s", c.baseURL, ip), nil), res); err != nil {
		return nil, err
	}
	return res, nil
}

// RegisterNextAvailable registers the next available IP address in a given cidr
func (c *Client) RegisterNextAvailable(ctx context.Context, cidr string, params *IPParams) (*UpdateIPResponse, error) {
	res := &UpdateIPResponse{}
	if err := c.sendRequest(
		mustNewRequest("POST", fmt.Sprintf("%s/api/v1/ip4/subnets/%s/register_next_available", c.baseURL, cidr), bytes.NewBuffer(mustMarshal(params))),
		res,
	); err != nil {
		return nil, err
	}
	return res, nil
}
