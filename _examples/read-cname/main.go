package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 2 {
		log.Fatal("Usage: script cname target")
	}
	cname := os.Args[1]

	log.Println("Reading for CNAME ", cname)

	dns, err := client.ReadCNAME(nil, cname)
	if err != nil {
		log.Fatal(err)
	}
	for _, item := range dns {
		log.Printf("%+v\n", item)
	}
}
