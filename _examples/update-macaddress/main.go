package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 3 {
		log.Fatal("Usage: script ip macaddress")
	}
	ip := os.Args[1]
	mac := os.Args[2]

	log.Printf("Updating mac of %s to %s\n", ip, mac)
	params := &aka.UpdateIPParams{
		MacAddress: mac,
	}
	res, err := client.UpdateIP(nil, *params, ip)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%+v", res)
}
