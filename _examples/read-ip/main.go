package main

import (
	"log"
	"os"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) < 2 {
		log.Fatal("Usage: get-ip.go ip [ip]...")
	} else {
		for _, item := range os.Args[1:] {
			ip, _ := client.ReadIP(nil, item)
			gout.MustPrint(ip)
		}
	}
}
