package main

import (
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 2 {
		panic("usage: script cname")
	}
	cname := os.Args[1]

	err := client.DeleteCNAME(nil, cname)
	if err != nil {
		panic(err)
	}
}
