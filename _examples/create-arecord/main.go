package main

import (
	"log"
	"log/slog"
	"os"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 3 {
		log.Fatal("Usage: script name ip")
	}
	name := os.Args[1]
	targetip := os.Args[2]

	slog.Info("reading ip", "name", name, "targetip", targetip)
	ip, err := client.ReadIP(nil, targetip)
	if err != nil {
		slog.Info("failed to get ip", "err", err)
	}
	gout.MustPrint(ip)
	params := &aka.DNSParams{
		Name:    name,
		Type:    "A",
		TTL:     300,
		Address: targetip,
	}
	item, err := client.CreateDNSRecord(nil, *params)
	if err != nil {
		slog.Warn("failed to create dns record", "err", err)
	}
	gout.MustPrint(item)
}
