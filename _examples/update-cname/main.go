package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 3 {
		log.Fatal("Usage: script cname target")
	}
	cname := os.Args[1]
	target := os.Args[2]

	log.Printf("Updating cname of %s to %s\n", cname, target)
	client.UpdateCNAME(nil, cname, target)
}
