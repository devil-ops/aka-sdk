package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) < 2 {
		log.Println("Looking up all addresses")
		subnets, err := client.ListSubnets(nil)
		if err != nil {
			log.Fatal(err)
		}
		for _, item := range *subnets {
			log.Printf("%+v", item)
		}
	} else {
		log.Println("Lookup up networks in args")
		for _, item := range os.Args[1:] {
			subnet, err := client.GetSubnet(nil, item)
			if err != nil {
				log.Fatal(err)
			}
			log.Printf("%+v", subnet)
		}
	}
}
