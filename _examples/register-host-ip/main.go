package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 3 {
		log.Fatal("Usage: script hostname ip")
	}
	host := os.Args[1]
	ip := os.Args[2]
	p := &aka.IPParams{
		Hostname: host,
		TTL:      3600,
	}
	res, err := client.CreateIP(nil, ip, p)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(res)
}
