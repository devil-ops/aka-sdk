package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	tagName := os.Args[1]
	log.Println("Looking up all subnets with tag:", tagName)
	subnets, err := client.GetSubnetByTag(nil, tagName)
	if err != nil {
		log.Fatal(err)
	}
	for i, item := range *subnets {
		log.Println(i, item)
	}

	if len(*subnets) == 0 {
		log.Println("No subnets found matching tag name:", tagName)
	}
}
