// This example adds a TAG to an aka subnet
package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	// Create the aka client config
	client := aka.MustNew()
	cidr := os.Args[1]
	tagName := os.Args[2]

	// Verify subnet exists
	_, err := client.GetSubnet(nil, cidr)
	if err != nil {
		log.Panicln("Error looking up subnet", cidr)
	}

	// delete tag on subnet
	log.Printf("Deleting tag:%+v to subnet: %s...\n", tagName, cidr)
	err = client.DeleteTag(nil, cidr, tagName)
	if err != nil {
		log.Panicf("failed to set tag: %s on subnet: %s", tagName, cidr)
	}
}
