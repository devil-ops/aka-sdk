package main

import (
	"log"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	zones, err := client.ListZones(nil)
	if err != nil {
		log.Fatal(err)
	}
	gout.MustPrint(zones)
}
