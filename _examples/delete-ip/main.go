package main

import (
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 2 {
		log.Fatal("Usage: script ip")
	}
	ip := os.Args[1]
	//p := &aka.IPParams{
	//Hostname: host,
	//TTL:      3600,
	//}
	res, err := client.DeleteIP(nil, ip)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(res)
}
