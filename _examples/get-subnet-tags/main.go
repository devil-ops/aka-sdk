package main

import (
	"log/slog"
	"os"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	cidr := os.Args[1]
	slog.Info("Retrieving all tags for subnet", "subnet", cidr)
	tags, err := client.GetSubnetTags(nil, cidr)
	if err != nil {
		panic(err)
	}
	if tags == nil {
		slog.Warn("No tags found for subnet", "subnet", cidr)
	}
	gout.MustPrint(tags)
}
