// This example adds a TAG to an aka subnet
package main

import (
	"log"
	"os"
	"strings"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	// Create the aka client config
	client := aka.MustNew()
	cidr := os.Args[1]
	tagArg := strings.Split(os.Args[2], "=")
	tag := aka.Tag{
		Name:  tagArg[0],
		Value: tagArg[1],
	}

	// Verify subnet exists
	_, err := client.GetSubnet(nil, cidr)
	if err != nil {
		log.Panicln("Error looking up subnet", cidr)
	}

	// Add tag to subnet
	log.Printf("Adding tag:%+v to subnet: %s...\n", tag, cidr)
	err = client.CreateTag(nil, cidr, tag)
	if err != nil {
		log.Panicf("failed to set tag:%+v on subnet: %s", tag, cidr)
	}
}
