package main

import (
	"log"
	"log/slog"
	"os"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) != 3 {
		log.Fatal("Usage: script cname target")
	}
	cname := os.Args[1]
	target := os.Args[2]

	// Check for cname
	existingRecords, err := client.GetDNSRecord(nil, cname)
	if err != nil {
		panic(err)
	}
	if len(existingRecords) > 0 {
		panic("record already exists")
	}

	slog.Info("creating cname", "cname", cname, "target", target)
	client.CreateCNAME(nil, cname, target)
}
