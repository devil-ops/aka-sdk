package main

import (
	"log/slog"
	"os"

	"github.com/drewstinnett/gout/v2"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func main() {
	client := aka.MustNew()
	if len(os.Args) < 2 {
		panic("Usage: get-dns.go name [name]...")
	} else {
		slog.Info("Lookup up names in args")
		for _, item := range os.Args[1:] {
			records, err := client.GetDNSRecord(nil, item)
			if err != nil {
				panic(err)
			}
			gout.MustPrint(records)
		}
	}
}
