package aka

import (
	"context"
	"errors"
)

const cnameS string = "CNAME"

// CreateCNAME generates a new cname entry
func (c *Client) CreateCNAME(ctx context.Context, cname string, target string) (*UpdateDNSResponse, error) {
	params := DNSParams{
		Name:   cname,
		Type:   cnameS,
		Target: target,
		TTL:    3600,
	}
	rec, err := c.CreateDNSRecord(ctx, params)
	if err != nil {
		return nil, err
	}
	return rec, nil
}

// ReadCNAME reads a cname
func (c *Client) ReadCNAME(ctx context.Context, cname string) (DNSRecords, error) {
	existingRecords, err := c.GetDNSRecord(ctx, cname)
	if err != nil {
		return nil, err
	}
	var items DNSRecords
	for _, record := range existingRecords {
		if record.Type == cnameS {
			items = append(items, record)
		}
	}

	return items, nil
}

// UpdateCNAME performs an update on a CNAME
func (c *Client) UpdateCNAME(ctx context.Context, cname string, target string) error {
	existingRecords, _ := c.GetDNSRecord(ctx, cname)
	if len(existingRecords) == 0 {
		return errors.New("no records exist here")
	}
	for _, record := range existingRecords {
		params := DNSParams{
			Name:   cname,
			Target: target,
			TTL:    record.TTL,
		}
		if record.Type == cnameS {
			if _, err := c.UpdateDNSRecord(ctx, params, record.EditPath); err != nil {
				return err
			}
		}
	}
	return nil
}

// DeleteCNAME removes a given cname
func (c *Client) DeleteCNAME(ctx context.Context, cname string) error {
	existingRecords, _ := c.GetDNSRecord(ctx, cname)
	if len(existingRecords) == 0 {
		return errors.New("no matching records")
	}
	for _, record := range existingRecords {
		if record.Type == "CNAME" {
			if _, err := c.DeleteDNSRecord(ctx, record.EditPath); err != nil {
				return err
			}
			c.Logger.Info("deleted", "record", record)
		}
	}
	return nil
}
