package aka

// Subnet is a subnet object from the api
type Subnet struct {
	Cidr          string      `json:"cidr,omitempty"`
	DHCPServers   DHCPServers `json:"dhcp_servers,omitempty"`
	Gateway       interface{} `json:"gateway,omitempty"`
	Name          interface{} `json:"name,omitempty"`
	SharedNetwork interface{} `json:"shared_network,omitempty"`
}

// Subnets is multiple subnet items
type Subnets []Subnet

// SubnetWithTags are just subnet items, but with the tags included
type SubnetWithTags struct {
	Subnet
	EffectiveTags map[string]string `json:"effective_tags,omitempty"`
}

// SubnetsWithTags are multiple SubnetWithTags options
type SubnetsWithTags []SubnetWithTags

// DHCPServer is a DHCP server
type DHCPServer struct {
	Name string `json:"name,omitempty"`
	IP   string `json:"ip,omitempty"`
}

// DHCPServers represent multiple DHCPServer items
type DHCPServers []DHCPServer

// Tag is a key value pair
type Tag struct {
	Name  string
	Value string
}
