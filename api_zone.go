package aka

import (
	"context"
	"fmt"
	"net/http"
)

// ListZones lists all zones in AKA
func (c *Client) ListZones(ctx context.Context) (*Zones, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/v1/dns/zones", c.baseURL), nil)
	if err != nil {
		return nil, err
	}
	items := &Zones{}
	if err := c.sendRequest(req, items); err != nil {
		return nil, err
	}
	return items, nil
}
