package aka

// DNSRecord is a dns record
type DNSRecord struct {
	Address         string `json:"address,omitempty"`
	BackendName     string `json:"backend_name,omitempty"`
	EditPath        string `json:"edit_path,omitempty"`
	Name            string `json:"name,omitempty"`
	Rdata           string `json:"rdata,omitempty"`
	TTL             int    `json:"ttl,omitempty"`
	Type            string `json:"type,omitempty"`
	View            string `json:"view,omitempty"`
	ZoneBackendName string `json:"zone_backend_name,omitempty"`
	ZoneName        string `json:"zone_name,omitempty"`
}

// DNSRecords is multiple DNSRecord items
type DNSRecords []DNSRecord

// UpdateDNSResponse is the info we get back from an update request
type UpdateDNSResponse struct {
	Alerts  []interface{} `json:"alerts,omitempty"`
	Notices []string      `json:"notices,omitempty"`
	Records DNSRecords    `json:"records,omitempty"`
}

// UpdateDNSResponseList is multiple UpdateDNSReponse items
type UpdateDNSResponseList []UpdateDNSResponse
