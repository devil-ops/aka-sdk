package aka

// Zone represents a DNS Zone
type Zone struct {
	BackendName         string      `json:"backend_name,omitempty"`
	DelegatedDNS        bool        `json:"delegated_dns,omitempty"`
	DNSServersPublished []DNSServer `json:"dns_servers_published,omitempty"`
	DNSServersUpstream  []DNSServer `json:"dns_servers_upstream,omitempty"`
	Name                string      `json:"name,omitempty"`
	View                string      `json:"view,omitempty"`
}

// DNSServer is a DNS Server
type DNSServer struct {
	Name string `json:"name,omitempty"`
	IP   string `json:"ip,omitempty"`
}

// Zones is multiple Zone items
type Zones []Zone
