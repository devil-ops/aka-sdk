package aka

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReadIP(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := io.Copy(w, bytes.NewReader([]byte(`{"ip":"10.138.20.84","subnet":"10.138.20.0/22","state":"static","mac_address":null,"last_registered":"2022-02-16T12:29:07.000-05:00","last_unregistered":"2021-12-18T14:50:20.000-05:00","last_seen":"2023-12-08T12:48:19.000-05:00"}`)))
		panicIfErr(err)
	}))
	c := MustNew(WithBaseURL(srv.URL), WithUsername("foo"), WithKey("bar"))
	got, err := c.ReadIP(context.TODO(), "10.138.20.84")
	require.NoError(t, err)
	require.NotNil(t, got)
	require.Equal(t, "10.138.20.84", got.IP)
}
