# Overview

[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/aka-sdk/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/aka-sdk/-/commits/main)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/aka-sdk/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/aka-sdk/-/commits/main)

Interact with the AKA API in a way that makes sense from a systems side.  This
means making some assumptions about default zones and such.  For example, if we
update a CNAME, we will do it to all applicable zones, unless otherwise
specified

## Using example scripts

The example scripts are the best place to learn right now

```shell
export AKA_USER=<your_username>
export AKA_KEY=<your_api_key>
```

If you would like to use aka-test, optionally do

```shell
export AKA_URL=https://aka-test.oit.duke.edu
```
