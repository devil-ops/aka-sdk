package aka

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	os.Clearenv()
	_, err := New()
	require.Error(t, err, "must set username")

	_, err = New(WithUsername("foo"))
	require.Error(t, err, "must set key")

	_, err = New(
		WithUsername("foo"),
		WithKey("bar"),
	)
	require.NoError(t, err)
}
