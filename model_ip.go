package aka

// IP represents an IP item from AKA
type IP struct {
	IP               string `json:"ip,omitempty"`
	LastRegistered   string `json:"last_registered,omitempty"`
	LastSeen         string `json:"last_seen,omitempty"`
	LastUnregistered string `json:"last_unregistered,omitempty"`
	MacAddress       string `json:"mac_address,omitempty"`
	State            string `json:"state,omitempty"`
	Subnet           string `json:"subnet,omitempty"`
}

// UpdateIPResponse is the response from IP interactions
type UpdateIPResponse struct {
	Alerts  []interface{} `json:"alerts,omitempty"`
	Notices []string      `json:"notices,omitempty"`
	Records DNSRecords    `json:"records,omitempty"`
}

// IPParams are parameters sent to the IP endpoint
type IPParams struct {
	Hostname   string `json:"hostname"`
	Views      string `json:"views,omitempty"`
	DHCP       bool   `json:"dhcp,omitempty"`
	MacAddress string `json:"mac_address,omitempty"`
	TTL        int    `json:"ttl"`
}

// UpdateIPParams are update parameters for IPs
type UpdateIPParams struct {
	MacAddress string `json:"mac_address,omitempty"`
	State      string `json:"state,omitempty"`
}
