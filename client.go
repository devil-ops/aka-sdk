/*
Package aka interacts with the AKA API at Duke University
*/
package aka

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"

	"moul.io/http2curl/v2"
)

/*
// AKAConfig is the configuration for an AKA client
type AKAConfig struct {
	BaseURL string
	APIUser string
	APIKey  string
	Logger  *slog.Logger
}

// MakeDefaults sets the defaults for AKA
func (cfg *AKAConfig) MakeDefaults() {
	if cfg.BaseURL == "" {
		envURL := os.Getenv("AKA_URL")
		if envURL != "" {
			cfg.BaseURL = envURL
		} else {
			cfg.BaseURL = "https://aka.oit.duke.edu"
		}
	}

	if cfg.APIUser == "" {
		envUser := os.Getenv("AKA_USER")
		if envUser == "" {
			panic("Must set the APIUser or use AKA_USER")
		} else {
			cfg.APIUser = envUser
		}
	}
	if cfg.APIKey == "" {
		envKey := os.Getenv("AKA_KEY")
		if envKey == "" {
			panic("Must set the APIKey or use AKA_KEY")
		} else {
			cfg.APIKey = envKey
		}
	}
}

*/

// Client is the thing that holds all the methods to interact with the API
type Client struct {
	// cfg        AKAConfig
	baseURL   string
	username  string
	key       string
	printCurl bool
	client    *http.Client
	Logger    *slog.Logger
}

// WithBaseURL sets the base url for a client
func WithBaseURL(s string) func(*Client) {
	return func(c *Client) {
		c.baseURL = s
	}
}

// WithUsername sets the username for an AKA client
func WithUsername(s string) func(*Client) {
	return func(c *Client) {
		c.username = s
	}
}

// WithKey sets the key for an AKA client
func WithKey(s string) func(*Client) {
	return func(c *Client) {
		c.key = s
	}
}

// MustNew returns a new client with functional options or panics on error
func MustNew(opts ...func(*Client)) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New uses functional options to create a new AKA client
func New(opts ...func(*Client)) (*Client, error) {
	// are we tryna' debug?
	c := &Client{
		baseURL:  "https://aka.oit.duke.edu",
		username: os.Getenv("AKA_USER"),
		key:      os.Getenv("AKA_KEY"),
		client: &http.Client{
			Timeout: time.Minute,
		},
	}
	if os.Getenv("PRINT_CURL") != "" {
		c.printCurl = true
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.username == "" {
		return nil, errors.New("must set a username")
	}
	if c.key == "" {
		return nil, errors.New("must set a key")
	}
	return c, nil
}

/*
// NewClient returns a new client with a given config
func NewClient(cfg *AKAConfig) *Client {
	return &Client{
		// cfg: *cfg,
		baseURL:  cfg.BaseURL,
		username: cfg.APIUser,
		key:      cfg.APIKey,
		client: &http.Client{
			Timeout: time.Minute,
		},
	}
}
*/

func (c *Client) sendRequest(req *http.Request, v interface{}) error {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.SetBasicAuth(c.username, c.key)

	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return err
	}

	defer dclose(res.Body)
	bodyText, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	if res.StatusCode == http.StatusUnauthorized {
		return errors.New("unauthorized, make user your username and password are correct")
	} else if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		fmt.Fprintf(os.Stderr, "%v", string(bodyText))
		return fmt.Errorf("bad request, returned: %+v", res.StatusCode)
	}

	if string(bodyText) != "" {
		if err = json.NewDecoder(bytes.NewReader(bodyText)).Decode(&v); err != nil {
			return err
		}
	} else {
		// When there is no body
		return nil
	}
	return nil
}

/*
// taken from sweet tutorial here:
// https://dev.to/plutov/writing-rest-api-client-in-go-3fkg
func (c *Client) sendRequestX(req *http.Request, v interface{}) ([]byte, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.SetBasicAuth(c.username, c.key)

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer dclose(res.Body)
	bodyText, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode == http.StatusUnauthorized {
		return nil, errors.New("unauthorized, make user your username and password are correct")
	} else if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		fmt.Fprintf(os.Stderr, "%v", string(bodyText))
		return nil, fmt.Errorf("bad request, returned: %+v", res.StatusCode)
	}
	return bodyText, nil
}

// NewClientWithEnv creates a new client using env variables
func NewClientWithEnv() (*Client, error) {
	user := os.Getenv("AKA_USER")
	key := os.Getenv("AKA_KEY")

	if user == "" {
		return nil, errors.New("AKA_USER must be set")
	}
	if key == "" {
		return nil, errors.New("AKA_KEY must be set")
	}
	baseURL := os.Getenv("AKA_URL")
	if baseURL == "" {
		baseURL = "https://aka.oit.duke.edu"
	}

	return NewClient(&AKAConfig{
		BaseURL: baseURL,
		APIUser: user,
		APIKey:  key,
	}), nil
}
*/

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprintf(os.Stderr, "error closing item: %v", err)
	}
}

func mustMarshal(v any) []byte {
	p, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return p
}

func mustNewRequest(m, u string, b io.Reader) *http.Request {
	r, err := http.NewRequest(m, u, b)
	if err != nil {
		panic(err)
	}
	return r
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
