package aka

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
)

// CreateTag creates a new tag on a given cidr
func (c *Client) CreateTag(ctx context.Context, cidr string, tag Tag) error {
	p, err := json.Marshal(map[string]string{
		"value": tag.Value,
	})
	if err != nil {
		return err
	}
	b := bytes.NewBuffer(p)

	uri := fmt.Sprintf("%s/api/v1/ip4/subnets/%s/tags/%s", c.baseURL, cidr, tag.Name)
	if err = c.sendRequest(mustNewRequest("POST", uri, b), nil); err != nil {
		return err
	}

	return nil
}

// DeleteTag deletes a tag
func (c *Client) DeleteTag(ctx context.Context, cidr string, tagName string) error {
	if err := c.sendRequest(
		mustNewRequest("DELETE", fmt.Sprintf("%s/api/v1/ip4/subnets/%s/tags/%s", c.baseURL, cidr, tagName), nil),
		nil,
	); err != nil {
		return err
	}

	return nil
}
