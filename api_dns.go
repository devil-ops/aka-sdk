package aka

import (
	"bytes"
	"context"
	"fmt"
)

// DNSParams are the parameters to use when interacting with the DNS endpoint
type DNSParams struct {
	Name     string `json:"name"`
	Type     string `json:"type"`
	TTL      int    `json:"ttl"`
	View     string `json:"view,omitempty"`
	Target   string `json:"target,omitempty"`
	Address  string `json:"address,omitempty"`
	Priority string `json:"priority,omitempty"`
	Weight   string `json:"weight,omitempty"`
	Service  string `json:"service,omitempty"`
	Protocol string `json:"protocol,omitempty"`
	RData    string `json:"rdata,omitempty"`
}

// GetDNSRecord returns a given dns record
func (c *Client) GetDNSRecord(ctx context.Context, name string) (DNSRecords, error) {
	var items DNSRecords
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/dns/%s", c.baseURL, name), nil),
		&items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetDNSWithZone returns DNS entries for a given zone
func (c *Client) GetDNSWithZone(ctx context.Context, zoneName string) (DNSRecords, error) {
	var items DNSRecords
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/dns/%s/children", c.baseURL, zoneName), nil),
		&items); err != nil {
		return nil, err
	}
	return items, nil
}

// GetDNSRecordSpecific This feels like the better function.  Should always return a single item
func (c *Client) GetDNSRecordSpecific(ctx context.Context, name string, view string) (*DNSRecord, error) {
	var items DNSRecords
	if err := c.sendRequest(
		mustNewRequest("GET", fmt.Sprintf("%s/api/v1/dns/%s", c.baseURL, name), nil),
		&items); err != nil {
		return nil, err
	}
	for _, record := range items {
		if record.View == view {
			return &record, nil
		}
	}
	return nil, fmt.Errorf("could not find record for %s in view %s", name, view)
}

// DeleteDNSRecord deletes a record from DNS
func (c *Client) DeleteDNSRecord(ctx context.Context, path string) (*UpdateDNSResponse, error) {
	res := &UpdateDNSResponse{}
	if err := c.sendRequest(
		mustNewRequest("DELETE", fmt.Sprintf("%s%s", c.baseURL, path), nil),
		res); err != nil {
		return nil, err
	}
	return res, nil
}

// CreateDNSRecord creates a new DNS record
func (c *Client) CreateDNSRecord(ctx context.Context, params DNSParams) (*UpdateDNSResponse, error) {
	req := mustNewRequest("POST", fmt.Sprintf("%s/api/v1/dns", c.baseURL), bytes.NewBuffer(mustMarshal(params)))

	res := &UpdateDNSResponse{}
	if err := c.sendRequest(req, res); err != nil {
		return nil, err
	}
	return res, nil
}

// UpdateDNSRecord updates a dns record
func (c *Client) UpdateDNSRecord(ctx context.Context, params DNSParams, path string) (*UpdateDNSResponse, error) {
	res := &UpdateDNSResponse{}
	if err := c.sendRequest(
		mustNewRequest("PUT", fmt.Sprintf("%s%s", c.baseURL, path), bytes.NewBuffer(mustMarshal(params))),
		res,
	); err != nil {
		return nil, err
	}
	return res, nil
}
